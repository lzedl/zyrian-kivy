# -*- coding: utf8 -*-
import yaml


with open('data/meta.yml') as fd:
    meta = yaml.load(fd)


class Player(object):
    def __init__(self):
        self.ship_type = meta['ship_type'][0]['name']
        self.front_gun = meta['front_gun'][0]['name']
        self.front_gun_level = 1
        self.rear_gun = ''
        self.rear_gun_level = 1
        self.generator = meta['generator'][0]['name']
        self.shield = meta['shield'][0]['name']
        self.left_sidekick = ''
        self.right_sidekick = ''


player = Player()
