# -*- coding: utf8 -*-
from kivy.uix.widget import Widget

from zyrian.utils import Sprite


class Background(Widget):
    def __init__(self, speed=10):
        super(Background, self).__init__()
        self.speed = speed
        source = 'data/images/space-back.png'
        self.image = Sprite(source=source)
        self.add_widget(self.image)
        self.size = self.image.size
        self.image_dupe = Sprite(source=source, y=self.height)
        self.add_widget(self.image_dupe)

    def update(self):
        self.image.y -= self.speed
        self.image_dupe.y -= self.speed

        if self.image.top <= 0:
            self.image.y += self.image.height
            self.image_dupe.y += self.image_dupe.height
