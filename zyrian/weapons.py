# -*- coding: utf8 -*-
from kivy.logger import Logger

from zyrian.bullets import PulseBullet
from zyrian.item import Item


class Weapon(Item):
    _level = 1
    max_cd = 0
    cd = 0
    damage = 0
    ship = None

    def __init__(self, bullet_class, levels, **ignore):
        self.bullet_class = bullet_class
        self.shot = self._shot_empty
        self.levels = levels

    @classmethod
    def create_from_meta(cls, name, section=None):
        meta_data = cls.get_item_meta(name, section)
        if meta_data:
            module_path, class_name = meta_data['class'].rsplit('.', 1)
            Logger.info('Create weapon %s (%s)', class_name, meta_data['class'])
            module = __import__(module_path, fromlist=True)
            weapon_class = getattr(module, class_name)

            return weapon_class(**meta_data)

    @property
    def level(self):
        return self._level

    @level.setter
    def level(self, value):
        self.set_level(value)

    def set_level(self, level):
        self._level = level
        self.shot = getattr(self, 'shot_%d' % level)
        self.damage = self.levels[level - 1]['damage']
        self.max_cd = self.levels[level - 1]['cool_down']

    def update(self):
        if self.cd:
            self.cd -= 1

    def fire(self):
        if self.cd == 0:
            self.shot()
            self.cd = self.max_cd

    def _shot_empty(self):
        pass


class NullGun(Weapon):
    def __init__(self, **ignore):
        super(NullGun, self).__init__(None, None)
        Logger.info('Create NullGun')

    def __repr__(self):
        return '<NullGun>'

    def set_level(self, level):
        pass


class PulseCannon(Weapon):
    def __init__(self, level=1, levels=None, **kwargs):
        super(PulseCannon, self).__init__(PulseBullet, levels, **kwargs)
        self.level = level

        Logger.info('PulseCanon created (level %d)', self.level)

    def __repr__(self):
        return '<PulseCannon: %d>' % self.level

    def shot_1(self):
        self.ship.parent.add_widget(self.bullet_class(center=self.ship.center))

    def shot_2(self):
        self.ship.parent.add_widget(self.bullet_class(center_x=self.ship.center_x-5,
                                                      center_y=self.ship.center_y))
        self.ship.parent.add_widget(self.bullet_class(center_x=self.ship.center_x+5,
                                                      center_y=self.ship.center_y))

    def shot_3(self):
        self.ship.parent.add_widget(self.bullet_class(center_x=self.ship.center_x-5,
                                                      center_y=self.ship.center_y))
        self.ship.parent.add_widget(self.bullet_class(center_x=self.ship.center_x+5,
                                                      center_y=self.ship.center_y))

        self.ship.parent.add_widget(self.bullet_class(center_x=self.ship.center_x-12,
                                                      center_y=self.ship.center_y-5))
        self.ship.parent.add_widget(self.bullet_class(center_x=self.ship.center_x+12,
                                                      center_y=self.ship.center_y-5))


class MultiCannon(PulseCannon):
    def __repr__(self):
        return '<MultiCannon: %d>' % self.level


class VulcanCannon(PulseCannon):
    def __repr__(self):
        return '<VulcanCannon: %d>' % self.level
