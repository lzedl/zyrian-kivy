# -*- coding: utf8 -*-
from zyrian.item import Item


class Shield(Item):
    meta_section = 'shield'

    power = 100
    max_power = 100
    energy_use = 1
    power_per_step = 1

    ship = None

    def __init__(self, name, max_power, energy_use, power_per_step, **ignore):
        self.name = name
        self.power = max_power
        self.max_power = max_power
        self.energy_use = energy_use
        self.power_per_step = power_per_step

    def update(self):
        if self.power < self.max_power and self.ship.energy >= self.energy_use:
            self.ship.energy -= self.energy_use
            self.power = min(self.power + self.power_per_step, self.max_power)

    def __repr__(self):
        return '<Shield: %s>' % self.name
