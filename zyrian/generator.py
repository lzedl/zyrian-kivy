# -*- coding: utf8 -*-
from zyrian.item import Item


class Generator(Item):
    meta_section = 'generator'

    energy = 100
    max_energy = 100
    energy_per_step = 1

    def __init__(self, name, max_energy, energy_per_step, **ignore):
        self.name = name
        self.energy = max_energy
        self.max_energy = max_energy
        self.energy_per_step = energy_per_step

    def update(self):
        self.energy = min(self.energy + self.energy_per_step, self.max_energy)

    def __repr__(self):
        return '<Generator: %s>' % self.name
