# -*- coding: utf8 -*-
from zyrian.utils import Sprite
from kivy.core.window import Window
from zyrian.objects import Asteroid


class Bullet(Sprite):
    speed = 10
    damage = 1

    def __init__(self, source, **kwargs):
        super(Bullet, self).__init__(source=source)
        for k, v in kwargs.iteritems():
            setattr(self, k, v)

    def update(self):
        self.y += self.speed
        if self.parent:
            for w in self.parent.children:
                if isinstance(w, Asteroid) and self.collide_widget(w):
                    w.hit(self.damage)
                    self.destroy()
            if self.y > Window.height:
                self.destroy()


class PulseBullet(Bullet):
    speed = 10

    def __init__(self, **kwargs):
        super(PulseBullet, self).__init__(source='data/images/pulse-bullet.png', **kwargs)
