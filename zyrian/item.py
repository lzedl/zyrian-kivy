# -*- coding: utf8 -*-
from zyrian.meta import meta


class Item(object):
    meta_section = None
    ship = None

    @classmethod
    def get_item_meta(cls, name, section=None):
        if section is None:
            section = cls.meta_section
        for item in meta[section]:
            if item['name'] == name:
                return item

    @classmethod
    def create_from_meta(cls, name, section=None):
        meta_data = cls.get_item_meta(name, section)
        if meta_data:
            return cls(**meta_data)
