# -*- coding: utf8 -*-
from random import randint, choice

from kivy.uix.widget import Widget
from kivy.core.window import Window
from kivy.clock import Clock
from kivy.uix.label import Label

from zyrian.ship import Ship
from zyrian.background import Background
from zyrian.objects import Asteroid
from zyrian.meta import player


class Hud(Label):
    template = 'Shield: %d/%d\nGenerator: %d/%d'

    def __init__(self, ship, **kwargs):
        super(Hud, self).__init__(text=self.template, **kwargs)
        self.ship = ship
        self.top = Window.height
        self.update()

    def update(self):
        self.text = self.template % (
            self.ship.shield.power,
            self.ship.shield.max_power,
            self.ship.generator.energy,
            self.ship.generator.max_energy
        )


class Game(Widget):
    def __init__(self, **kwargs):
        super(Game, self).__init__(**kwargs)
        self.background = Background()
        Window.size = self.background.size
        self.add_widget(self.background)

        self.ship = Ship(center_x=self.background.center_x)
        self.ship.front_gun = player.front_gun
        self.ship.front_gun.level = 3
        self.ship.rear_gun = player.rear_gun
        self.ship.shield = player.shield
        self.ship.generator = player.generator
        self.add_widget(self.ship)

        self.add_widget(Hud(self.ship))

        Clock.schedule_interval(self.update, 1.0/60.0)
        Clock.schedule_interval(self.create_asteroid, 1.0/randint(1, 5))

    def update(self, dt):
        for child in list(self.children):
            child.update()

    def create_asteroid(self, dt):
        a = Asteroid(choice(['s', 'm', 'l']), y=Window.height, center_x=randint(0, Window.width))
        self.add_widget(a)
