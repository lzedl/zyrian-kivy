# -*- coding: utf8 -*-
from kivy.uix.widget import Widget
from kivy.uix.button import Button

from zyrian.meta import meta, player


class Menu(Widget):
    def change_main_widget(self, widget):
        parent = self.parent
        parent.remove_widget(self)
        parent.add_widget(widget)


class MainMenu(Menu):
    def start_game(self):
        from zyrian.game import Game

        self.change_main_widget(Game())

    def shop(self):
        self.change_main_widget(ShopCategoriesMenu())


class ShopCategoriesMenu(Menu):
    def back(self):
        self.change_main_widget(MainMenu())

    def go_to_shop(self, shop_type):
        self.change_main_widget(ShopMenu(shop_type))


class ShopMenu(Menu):
    def __init__(self, shop_type):
        super(ShopMenu, self).__init__()

        self.shop_type = shop_type

        self.set_items(meta[shop_type], getattr(player, shop_type))

    def set_items(self, items, current):
        layout = self.ids['ShopMenuLayout']

        for item in items:
            button = Button(text=item['name'], on_press=lambda b: self.set_item(b.text))
            if item['name'] == current:
                button.bold = True
                button.italic = True

            layout.add_widget(button, index=1)

    def set_item(self, item):
        setattr(player, self.shop_type, item)
        self.back()

    def back(self):
        self.change_main_widget(ShopCategoriesMenu())
