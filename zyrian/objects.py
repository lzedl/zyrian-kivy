# -*- coding: utf8 -*-
from zyrian.utils import Sprite


SIZE_TO_HP = {
    's': 2,
    'm': 8,
    'l': 15
}


class Asteroid(Sprite):
    damage = 1

    def __init__(self, size='s', **kwargs):
        super(Asteroid, self).__init__(source='data/images/asteroid-%s.png' % size)
        for k, v in kwargs.iteritems():
            setattr(self, k, v)

        self.hp = SIZE_TO_HP[size]

    def update(self):
        from zyrian.ship import Ship

        self.y -= 6
        if self.parent:
            for obj in self.parent.children:
                if isinstance(obj, Ship) and self.collide_widget(obj):
                    obj.hit(self.hp*2)
                    self.destroy()
            if self.top < 0:
                self.destroy()

    def hit(self, damage):
        self.hp -= damage
        if self.hp <= 0:
            self.destroy()
