# -*- coding: utf8 -*-
from collections import defaultdict

from kivy.core.window import Window, Keyboard
from kivy.logger import Logger

from zyrian.utils import Sprite
from zyrian.weapons import Weapon, NullGun
from zyrian.shield import Shield
from zyrian.generator import Generator


class Ship(Sprite):
    armor = 100
    speed = 3
    _front_gun = None
    _rear_gun = None
    _shield = None
    _generator = None
    _left_sidekick = None
    _right_sidekick = None

    def __init__(self, **kwargs):
        super(Ship, self).__init__(source='data/images/red-ship-idle.png')
        for k, v in kwargs.iteritems():
            setattr(self, k, v)
        Window.bind(on_key_down=self.on_key_down, on_key_up=self.on_key_up)
        self.keys = defaultdict(bool)

        Logger.info('Ship created (%d, %d)', self.x, self.y)

    def on_key_down(self, window, keycode, *rest):
        self.keys[keycode] = True

    def on_key_up(self, window, keycode, *rest):
        self.keys[keycode] = False

    def update(self):
        self.generator.update()
        self.front_gun.update()
        self.shield.update()

        if self.keys[Keyboard.keycodes['left']]:
            self.x -= self.speed
        elif self.keys[Keyboard.keycodes['right']]:
            self.x += self.speed

        if self.keys[Keyboard.keycodes['up']]:
            self.y += self.speed
        elif self.keys[Keyboard.keycodes['down']]:
            self.y -= self.speed

        if self.keys[Keyboard.keycodes['lctrl']]:
            self._front_gun.fire()
            self._rear_gun.fire()
            # self._left_sidekick.fire()
            # self._right_sidekick.fire()

    @property
    def front_gun(self):
        return self._front_gun

    @front_gun.setter
    def front_gun(self, value):
        if isinstance(value, basestring):
            self._front_gun = Weapon.create_from_meta(value, 'front_gun')
        else:
            self._front_gun = value

        if self._front_gun:
            self._front_gun.ship = self
        else:
            self._front_gun = NullGun()

        Logger.info('Set front gun: %s', self._front_gun)

    @property
    def rear_gun(self):
        return self._front_gun

    @rear_gun.setter
    def rear_gun(self, value):
        if isinstance(value, basestring):
            self._rear_gun = Weapon.create_from_meta(value, 'rear_gun')
        else:
            self._rear_gun = value

        if self._rear_gun:
            self._rear_gun.ship = self
        else:
            self._rear_gun = NullGun()

        Logger.info('Set rear gun: %s', self._rear_gun)

    @property
    def shield(self):
        return self._shield

    @shield.setter
    def shield(self, value):
        if isinstance(value, basestring):
            self._shield = Shield.create_from_meta(value)
        else:
            self._shield = value

        if self._shield:
            self._shield.ship = self
        Logger.info('Set ship shield: %s', self._shield)

    def hit(self, damage):
        if self.shield.power > damage:
            self.shield.power -= damage
        else:
            damage -= self.shield.power
            self.armor -= damage

    @property
    def generator(self):
        return self._generator

    @generator.setter
    def generator(self, value):
        if isinstance(value, basestring):
            self._generator = Generator.create_from_meta(value)
        else:
            self._generator = value

        if self._generator:
            self._generator.ship = self

        Logger.info('Set ship generator: %s', self._generator)

    @property
    def energy(self):
        return self.generator.energy

    @energy.setter
    def energy(self, value):
        self.generator.energy = value
