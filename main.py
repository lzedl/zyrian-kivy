# -*- coding: utf8 -*-
import kivy

kivy.require('1.1.1')

from kivy.app import App
from zyrian.menu import MainMenu


class ZyrianApp(App):
    title = 'test'

    def build(self):
        return MainMenu()


if __name__ == '__main__':
    ZyrianApp().run()
